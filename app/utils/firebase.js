import firebase from "firebase/app"

const firebaseConfig = {
    apiKey: "AIzaSyCD68TqMBzn3wfO8uHhnyCAfr-L4o06n-s",
    authDomain: "rn-crud-7614a.firebaseapp.com",
    databaseURL: "https://rn-crud-7614a.firebaseio.com",
    projectId: "rn-crud-7614a",
    storageBucket: "rn-crud-7614a.appspot.com",
    messagingSenderId: "617817554260",
    appId: "1:617817554260:web:0d4ce0f0b5358851521cc1",
    measurementId: "G-MF52CFT2KH"
};

// Initialize Firebase
export const firebaseApp = firebase.initializeApp(firebaseConfig);