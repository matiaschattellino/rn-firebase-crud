import React from 'react'
import { View, StyleSheet, Text, ScrollView  } from "react-native";


const UserLogged = () => {
    return ( 
        <ScrollView>
            <Text style={styles.title}>Sign in with your account</Text>
            <View style={styles.viewBtn}>
                <Button
                    title="Sign In"
                    buttonStyle={styles.btnStyle}
                    containerStyle={styles.btnContainer}
                    onPress={() => console.log("sign in")}
                />
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({

});

export default UserLogged;