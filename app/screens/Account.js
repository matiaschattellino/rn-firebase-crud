import React, { useState, useEffect } from 'react'
import { View, StyleSheet, Text } from "react-native";
import * as firebase from 'firebase';
import UserGuest from './UserGuest';
import Login from './Login'

const AccountScreen = ({ navigation }) => {
    console.log(navigation)
    const [ loggin, setLoggin ] = useState(null);

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            !user ? setLoggin(false) : setLoggin(true)
        })
        
    }, []);

    if(loggin === null) return <Text>Loading...</Text>

   return <Login navigation={navigation} />
};

const styles = StyleSheet.create({

});

export default AccountScreen;