import React, { useState } from 'react';
import { View, StyleSheet } from "react-native";
import { Button, Input, Icon } from 'react-native-elements';
import * as firebase from 'firebase';
import Loading from './Loading';

const Form = ({ navigation }) => {
    const [ formData, setFormData ] = useState(defaultFormValue());
    const [ loading, setIsLoading ] = useState(false);

    const onSubmit = () => {
        setIsLoading(true);
        firebase
            .auth()
            .createUserWithEmailAndPassword(formData.email, formData.password)
            .then(res => {
                setIsLoading(false)
                navigation.navigate("Login")
            })
            .catch(err => {
                setIsLoading(false)
                console.log(err);
            })
    };

    const onChange = (e, type) => {
        setFormData({ ...formData, [type]: e.nativeEvent.text })
    }

    return ( 
        <View style={styles.formContainer}>
            <Input 
                placeholder="E-mail"
                containerStyle={styles.inputForm}
                onChange={e => onChange(e, "email")}

            />
            <Input 
                placeholder="Password"
                containerStyle={styles.inputForm}
                password={true}
                secureTextEntry={true}
                onChange={e => onChange(e, "password")}
            />
            <Button 
                title="Register"
                containerStyle={styles.btnContainerRegister}
                buttonStyle={styles.btnRegister}
                onPress={onSubmit}
            />
            <Loading isVisible={loading} text="Create Acount" />
        </View>
     );
}

function defaultFormValue() {
    return {
        email: "",
        password: ""
    }
}

const styles = StyleSheet.create({
    formContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30
    },
    inputForm: {
        width: "100%",
        marginTop: 20
    },
    btnContainerRegister: {
        marginTop: 20,
        width: "95%"
    },
    btnRegister: {
        backgroundColor: "#00a680"
    }
})
 
export default Form;