import React from 'react';
import { View, StyleSheet, Text, ScrollView } from "react-native";
import { Button } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Form from './Form'

const Login = ({ navigation }) => {
    return ( 
        <KeyboardAwareScrollView>
            <Form navigation={navigation} />
        </KeyboardAwareScrollView>
     );
};

const styles = StyleSheet.create({

});
 
export default Login;