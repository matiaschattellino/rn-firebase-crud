import React from 'react';
import { View, StyleSheet, Text, ScrollView } from "react-native";
import { Button } from 'react-native-elements';
import LoginForm from './LoginForm';

const Login = ({ navigation }) => {
    return ( 
        <ScrollView>
            <View style={styles.viewContainer}>
                <LoginForm navigation={navigation} />
            </View>
            <View style={styles.viewContainer}>
            <Text 
                style={styles.textRegister}>
                    You dont have account?{" "}
                    <Text 
                        style={styles.btnRegister}
                        onPress={() => navigation.navigate('SignIn')}
                    >
                        Create Account
                    </Text>
            </Text>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({   
    viewContainer: {
        marginRight: 40,
        marginLeft: 40
    },
    textRegister: {
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10
    },
    btnRegister: {
        color: "#00a680",
        fontWeight: "bold"
    }
})
 
export default Login;