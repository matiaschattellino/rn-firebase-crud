import React, { useState } from 'react';
import { View, StyleSheet } from "react-native";
import { Button, Input, Icon } from 'react-native-elements';
import * as firebase from 'firebase';
import Loading from './Loading'

const LoginForm = ({ navigation }) => {
    const [ formData, setFormData ] = useState(defaultFormValue());
    const [ loading, setIsLoading ] = useState(false);

    const onChange = (e, type) => {
        setFormData({ ...formData, [type]: e.nativeEvent.text})
    }

    const onSubmit = () => {
        setIsLoading(true);
        firebase
            .auth()
            .signInWithEmailAndPassword(formData.email, formData.password)
            .then(() => {
                setIsLoading(false);
                navigation.navigate("Home")
            })
            .catch(() => {
                console.log("password wrong")
                setIsLoading(false);
            })
    }

    return ( 
        <View style={styles.formContainer}>
            <Input 
                placeholder="email"
                containerStyle={styles.inputForm}
                onChange={(e) => onChange(e, "email")}
            />
              <Input 
                placeholder="password"
                containerStyle={styles.inputForm}
                password={true}
                secureTextEntry={true}
                onChange={(e) => onChange(e, "password")}
            />
            <Button 
                title="Login"
                containerStyle={styles.btnContainerLogin}
                buttonStyle={styles.btnLogin}
                onPress={onSubmit}
            />
        </View>
     );
};

function defaultFormValue() {
    return {
        email: "",
        password: ""
    }
}

const styles = StyleSheet.create({
    formContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 30
    },
    inputForm: {
        width: "100%",
        marginTop: 20
    },
    btnContainerLogin: {
        marginTop: 20,
        width: "95%"
    },
    btnLogin: {
        backgroundColor: "#00a680"
    }
})
 
export default LoginForm;