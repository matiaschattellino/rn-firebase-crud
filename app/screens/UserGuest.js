import React from 'react'
import { View, StyleSheet, Text, ScrollView } from "react-native";
import { Button } from 'react-native-elements';


const UserGuest = () => {
    return ( 
        <ScrollView centerContent={true} style={styles.viewBody}>
            <Text style={styles.title}>Create your account</Text>
            <View style={styles.viewBtn}>
                <Button
                    title="Login"
                    buttonStyle={styles.btnStyle}
                    containerStyle={styles.btnContainer}
                    onPress={() => console.log("creating account")}
                />
            </View>
            <View style={styles.viewBtn}>
                  <Button
                    title="SignUp"
                    buttonStyle={styles.btnStyle}
                    containerStyle={styles.btnContainer}
                    onPress={() => console.log("sign in")}
                />
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    viewBody: {
        marginLeft: 30,
        marginRight: 30,
        marginTop: 15
    },
    title: {
        fontWeight: "bold",
        fontSize: 19,
        marginBottom: 10,
        textAlign: "center"
    },
    viewBtn: {
        flex: 1,
        alignItems: "center",
        marginBottom: 15,
    },
    btnStyle: {
        backgroundColor: "#00a680"
    },
    btnContainer: {
        width: "70%",

    }
});

export default UserGuest;