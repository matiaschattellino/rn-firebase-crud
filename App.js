import React, { useEffect } from 'react';
import { View, Button, Text, StyleSheet } from "react-native"
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import { firebaseApp } from "./app/utils/firebase";
import AccountScreen from './app/screens/Account';
import LoginScreen from './app/screens/Login';
import SignInScreen from './app/screens/SignIn';
import HomeScreen from './app/screens/HomeScreen'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
        <Drawer.Navigator>
            <Drawer.Screen name="Home" component={HomeScreen} />
        </Drawer.Navigator>       
  );
};


const StackNavigation = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="SignIn" component={SignInScreen} />
          <Stack.Screen name="Home" component={DrawerNavigator} />
      </Stack.Navigator>
  ) 
}

export default function App() {
  return (
    <NavigationContainer>
        <StackNavigation />
    </NavigationContainer>
  );
};